def incrementVersion() {
    echo "incrementation the version ..."
    sh 'mvn build-helper:parse-version versions:set \
        -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
        versions:commit'
        def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
        def version = matcher[0][1]
        env.IMAGE_NAME = "$version-$BUILD_NUMBER"
}


def buildJar() {
    echo "building the application..."
    sh 'mvn clean package'
} 

def buildImage() {
    echo "building docker image"
    withCredentials([usernamePassword(credentialsId: 'docker-cred', usernameVariable: 'USER', passwordVariable: 'PWD')]) {
        sh """
        docker build -t abdelhakim1984/java-maven-app:$IMAGE_NAME .
        echo $PWD | docker login -u $USER --password-stdin
        docker push abdelhakim1984/java-maven-app:$IMAGE_NAME
        """
    }
} 

def deployApp() {
    echo 'deploying the application...'
    
}

def commitVersion() {
    echo 'increment the version of  pom.xml'
    withCredentials([usernamePassword(credentialsId: 'gitlab-cred', usernameVariable: 'USER', passwordVariable: 'PWD')]){
        //sh 'git config --global user.email "jenkins@example.com"'
        //sh 'git config --global user.name "jenkins"'
        sh "git remote set-url origin https://${USER}:${PWD}@gitlab.com/Abdelhakim-Aouay/java-maven-app.git"
        sh 'git add .'
        sh 'git commit -m "ci: version bump"'
        sh 'git push origin HEAD:master'
    }
    
}
return this
